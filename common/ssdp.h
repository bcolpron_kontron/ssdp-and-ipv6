#include <stdio.h>      // snprintf, vsnprintf
#include <stdlib.h>     // malloc, free
#include <stdarg.h>     // va_start, va_end, va_list
#include <string.h>     // memset, memcpy, strlen, strcpy, strcmp, strncasecmp, strerror
#include <ctype.h>      // isprint, isspace
#include <errno.h>      // errno
#include <unistd.h>     // close
#include <sys/time.h>   // gettimeofday
#include <sys/ioctl.h>  // ioctl, FIONBIO
#include <net/if.h>     // struct ifconf, struct ifreq
#include <fcntl.h>      // fcntl, F_GETFD, F_SETFD, FD_CLOEXEC
#include <sys/socket.h> // struct sockaddr, AF_INET, SOL_SOCKET, socklen_t, setsockopt, socket, bind, sendto, recvfrom
#include <ifaddrs.h>    //struct ifaddrs,getifaddrs()
#include <netinet/in.h> // struct sockaddr_in, struct ip_mreq, INADDR_ANY, IPPROTO_IP, also include <sys/socket.h>
#include <arpa/inet.h>  // inet_aton, inet_ntop, inet_addr, also include <netinet/in.h>

/** Definition **/
// LSSDP Log Level
enum LSSDP_LOG {
    LSSDP_LOG_DEBUG = 1 << 0,
    LSSDP_LOG_INFO  = 1 << 1,
    LSSDP_LOG_WARN  = 1 << 2,
    LSSDP_LOG_ERROR = 1 << 3
};

#define LSSDP_BUFFER_LEN    2048
#define lssdp_debug(fmt, agrs...) lssdp_log(LSSDP_LOG_DEBUG, __LINE__, __func__, fmt, ##agrs)
#define lssdp_info(fmt, agrs...)  lssdp_log(LSSDP_LOG_INFO,  __LINE__, __func__, fmt, ##agrs)
#define lssdp_warn(fmt, agrs...)  lssdp_log(LSSDP_LOG_WARN,  __LINE__, __func__, fmt, ##agrs)
#define lssdp_error(fmt, agrs...) lssdp_log(LSSDP_LOG_ERROR, __LINE__, __func__, fmt, ##agrs)


/* Struct : lssdp_ctx */
#define LSSDP_FIELD_LEN         128
#define LSSDP_LOCATION_LEN      256
#define LSSDP_INTERFACE_NAME_LEN    16                      // IFNAMSIZ
#define LSSDP_INTERFACE_LIST_SIZE   16
#define LSSDP_IP_LEN                40

struct lssdp_interface {
    char        name        [LSSDP_INTERFACE_NAME_LEN]; // name[16]
    char        ip          [LSSDP_IP_LEN];             // ip[16] = "xxx.xxx.xxx.xxx"
    __uint128_t    addr;                                   // address in network byte order
    __uint128_t    netmask;                                // mask in network byte order
    uint32_t       scope_id;
};

typedef struct lssdp_ctx {
    int             sock;                                   // SSDP socket
    unsigned short  port;                                   // SSDP port (0x0000 ~ 0xFFFF)
    bool            debug;                                  // show debug log

    /* Network Interface */
    size_t          interface_num;                          // interface number
    lssdp_interface interface[LSSDP_INTERFACE_LIST_SIZE];                 // interface[16]

    /* SSDP Header Fields */
    struct {
        /* SSDP Standard Header Fields */
        char        server              [LSSDP_FIELD_LEN];  // Server field
        char        search_target       [LSSDP_FIELD_LEN];  // Search Target
        char        unique_service_name [LSSDP_FIELD_LEN];  // Unique Service Name: MAC or User Name
        struct {                                            // Location (optional):
            char    prefix              [LSSDP_FIELD_LEN];  // Protocal: "https://" or "http://"
            char    domain              [LSSDP_FIELD_LEN];  // if domain is empty, using Interface IP as default
            char    suffix              [LSSDP_FIELD_LEN];  // URI or Port: "/index.html" or ":80"
        } location;

        /* Additional SSDP Header Fields */
        char        sm_id       [LSSDP_FIELD_LEN];
        char        device_type [LSSDP_FIELD_LEN];
    } header;

    /* Callback Function */
    int (* network_interface_changed_callback) (struct lssdp_ctx * lssdp);
    int (* packet_received_callback)           (struct lssdp_ctx * lssdp, const char * packet, size_t packet_len);

} lssdp_ctx;



/** Struct: lssdp_packet **/
typedef struct lssdp_packet {
    char            method      [LSSDP_FIELD_LEN];      // M-SEARCH, NOTIFY, RESPONSE
    char            st          [LSSDP_FIELD_LEN];      // Search Target
    char            usn         [LSSDP_FIELD_LEN];      // Unique Service Name
    char            location    [LSSDP_LOCATION_LEN];   // Location
    char            server      [LSSDP_LOCATION_LEN];

    /* Additional SSDP Header Fields */
    char            sm_id       [LSSDP_FIELD_LEN];
    char            device_type [LSSDP_FIELD_LEN];
    long long       update_time;
} lssdp_packet;


/** Internal Function **/
static int send_multicast_data(const char * data, const struct lssdp_interface interface, unsigned short ssdp_port);
static int lssdp_send_response(lssdp_ctx * lssdp, struct sockaddr_in6 address);
static int lssdp_packet_parser(const char * data, size_t data_len, lssdp_packet * packet);
static int parse_field_line(const char * data, size_t start, size_t end, lssdp_packet * packet);
static int get_colon_index(const char * string, size_t start, size_t end);
static int trim_spaces(const char * string, size_t * start, size_t * end);
static long long get_current_time_();
static int lssdp_log(int level, int line, const char * func, const char * format, ...);
static struct lssdp_interface * find_interface_in_LAN(lssdp_ctx * lssdp, sockaddr_in6 address);
int lssdp_socket_close(lssdp_ctx * lssdp);


/** Global Variable **/
static struct {
    const char * MSEARCH;
    const char * NOTIFY;
    const char * RESPONSE;

    const char * HEADER_MSEARCH;
    const char * HEADER_NOTIFY;
    const char * HEADER_RESPONSE;

    const char * ADDR_LOCALHOST;
    const char * ADDR_MULTICAST;

    void (* log_callback)(const char * file, const char * tag, int level, int line, const char * func, const char * message);

} Global = {
    // SSDP Method
    .MSEARCH  = "M-SEARCH",
    .NOTIFY   = "NOTIFY",
    .RESPONSE = "RESPONSE",

    // SSDP Header
    .HEADER_MSEARCH  = "M-SEARCH * HTTP/1.1\r\n",
    .HEADER_NOTIFY   = "NOTIFY * HTTP/1.1\r\n",
    .HEADER_RESPONSE = "HTTP/1.1 200 OK\r\n",

    // IP Address
    .ADDR_LOCALHOST = "::1",
    .ADDR_MULTICAST = "FF02::C",

    // Log Callback
    .log_callback = NULL
};


// 01. lssdp_network_interface_update
int lssdp_network_interface_update(lssdp_ctx * lssdp) {
    if (lssdp == NULL) {
        lssdp_error("lssdp should not be NULL\n");
        return -1;
    }

    const size_t SIZE_OF_INTERFACE_LIST = sizeof(struct lssdp_interface) * LSSDP_INTERFACE_LIST_SIZE;

    // 1. copy orginal interface
    struct lssdp_interface original_interface[LSSDP_INTERFACE_LIST_SIZE] = {};
    memcpy(original_interface, lssdp->interface, SIZE_OF_INTERFACE_LIST);

    // 2. reset lssdp->interface
    lssdp->interface_num = 0;
    memset(lssdp->interface, 0, SIZE_OF_INTERFACE_LIST);

    int result = -1;

    struct ifaddrs *ifaddr, *ifa;
    if (getifaddrs(&ifaddr) < 0) {
        lssdp_error("getifaddrs() failed, errno = %s (%d)\n", strerror(errno), errno);
        goto end;
    }

    // 5. setup lssdp->interface
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr == NULL) { continue; }


        if (ifa->ifa_addr->sa_family != AF_INET6) {
            // only support IPv6
            continue;
        }

        // 5-1. get interface ip string
        char ip[LSSDP_IP_LEN] = {};
        struct sockaddr_in6 *a = (struct sockaddr_in6 *)ifa->ifa_addr;
        if (inet_ntop(AF_INET6, &(a->sin6_addr), ip, sizeof(ip)) == NULL) {
            lssdp_error("inet_ntop failed, errno = %s (%d)\n", strerror(errno), errno);
            continue;
        }

        // 5-3. check network interface number
        if (lssdp->interface_num >= LSSDP_INTERFACE_LIST_SIZE) {
            lssdp_warn("interface number is over than MAX SIZE (%d)     %s %s\n", LSSDP_INTERFACE_LIST_SIZE, ifa->ifa_name, ip);
            continue;
        }

        // 5-4. set interface
        size_t n = lssdp->interface_num;
        snprintf(lssdp->interface[n].name, LSSDP_INTERFACE_NAME_LEN, "%s", ifa->ifa_name); // name
        snprintf(lssdp->interface[n].ip,   LSSDP_IP_LEN,             "%s", ip);            // ip string
        memcpy(&lssdp->interface[n].addr, a->sin6_addr.s6_addr, 16);                                  // address in network byte order

        // set network mask
        memcpy(&lssdp->interface[n].netmask, ((struct sockaddr_in6 *)ifa->ifa_netmask)->sin6_addr.s6_addr, 16);                               // mask in network byte order
        lssdp->interface[n].scope_id = ((struct sockaddr_in6 *)ifa->ifa_netmask)->sin6_scope_id;

        // increase interface number
        lssdp->interface_num++;
    }

    result = 0;
end:

    // compare with original interface
    if (memcmp(original_interface, lssdp->interface, SIZE_OF_INTERFACE_LIST) == 0) {
        // interface is not changed
        return result;
    }

    /* Network Interface is changed */

    // 2. invoke network interface changed callback
    if (lssdp->network_interface_changed_callback != NULL) {
        lssdp->network_interface_changed_callback(lssdp);
    }

    return result;
}

// 02. lssdp_socket_create
int lssdp_socket_create(lssdp_ctx * lssdp) {
    if (lssdp == NULL) {
        lssdp_error("lssdp should not be NULL\n");
        return -1;
    }

    if (lssdp->port == 0) {
        lssdp_error("SSDP port (%d) has not been setup.\n", lssdp->port);
        return -1;
    }

    // close original SSDP socket
    lssdp_socket_close(lssdp);

    // create UDP socket
    lssdp->sock = socket(PF_INET6, SOCK_DGRAM, 0);
    if (lssdp->sock < 0) {
        lssdp_error("create socket failed, errno = %s (%d)\n", strerror(errno), errno);
        return -1;
    }

    int result = -1;
    int sock_opt;
    ipv6_mreq imr{};
    sockaddr_in6 addr{};

    // set non-blocking
    int opt = 1;
    if (ioctl(lssdp->sock, FIONBIO, &opt) != 0) {
        lssdp_error("ioctl FIONBIO failed, errno = %s (%d)\n", strerror(errno), errno);
        goto end;
    }

    // set reuse address
    if (setsockopt(lssdp->sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) != 0) {
        lssdp_error("setsockopt SO_REUSEADDR failed, errno = %s (%d)\n", strerror(errno), errno);
        goto end;
    }

    // set FD_CLOEXEC (http://kaivy2001.pixnet.net/blog/post/32726732)
    sock_opt = fcntl(lssdp->sock, F_GETFD);
    if (sock_opt == -1) {
        lssdp_error("fcntl F_GETFD failed, errno = %s (%d)\n", strerror(errno), errno);
    } else {
        // F_SETFD
        if (fcntl(lssdp->sock, F_SETFD, sock_opt | FD_CLOEXEC) == -1) {
            lssdp_error("fcntl F_SETFD FD_CLOEXEC failed, errno = %s (%d)\n", strerror(errno), errno);
        }
    }

    // bind socket
    addr.sin6_family    = AF_INET6;
    addr.sin6_port      = htons(lssdp->port);
    addr.sin6_addr      = in6addr_any;
    if (bind(lssdp->sock, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
        lssdp_error("bind failed, errno = %s (%d)\n", strerror(errno), errno);
        goto end;
    }

    // set IP_ADD_MEMBERSHIP
    imr.ipv6mr_interface = 0;
    if (inet_pton(AF_INET6, Global.ADDR_MULTICAST, &imr.ipv6mr_multiaddr) <= 0) {
        lssdp_error("inet_pton failed, errno = %s (%d)\n", strerror(errno), errno);
        goto end;
    }

    if (setsockopt(lssdp->sock, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, &imr, sizeof(imr)) != 0) {
        lssdp_error("setsockopt IPV6_ADD_MEMBERSHIP failed: %s (%d)\n", strerror(errno), errno);
        goto end;
    }

    lssdp_info("created SSDP socket %d\n", lssdp->sock);
    result = 0;
end:
    if (result == -1) {
        lssdp_socket_close(lssdp);
    }
    return result;
}

// 03. lssdp_socket_close
int lssdp_socket_close(lssdp_ctx * lssdp) {
    if (lssdp == NULL) {
        lssdp_error("lssdp should not be NULL\n");
        return -1;
    }

    // check lssdp->sock
    if (lssdp->sock <= 0) {
        lssdp_warn("SSDP socket is %d, ignore socket_close request.\n", lssdp->sock);
        goto end;
    }

    // close socket
    if (close(lssdp->sock) != 0) {
        lssdp_error("close socket %d failed, errno = %s (%d)\n", lssdp->sock, strerror(errno), errno);
        return -1;
    };

    // close socket success
    lssdp_info("close SSDP socket %d\n", lssdp->sock);
end:
    lssdp->sock = -1;
    return 0;
}

// 04. lssdp_socket_read
int lssdp_socket_read(lssdp_ctx * lssdp) {
    if (lssdp == NULL) {
        lssdp_error("lssdp should not be NULL\n");
        return -1;
    }

    // check socket and port
    if (lssdp->sock <= 0) {
        lssdp_error("SSDP socket (%d) has not been setup.\n", lssdp->sock);
        return -1;
    }

    if (lssdp->port == 0) {
        lssdp_error("SSDP port (%d) has not been setup.\n", lssdp->port);
        return -1;
    }

    char buffer[LSSDP_BUFFER_LEN] = {};
    sockaddr_in6 address {};
    socklen_t address_len = sizeof(address);

    ssize_t recv_len = recvfrom(lssdp->sock, buffer, sizeof(buffer), 0, (struct sockaddr *)&address, &address_len);
    if (recv_len == -1) {
        lssdp_error("recvfrom fd %d failed, errno = %s (%d)\n", lssdp->sock, strerror(errno), errno);
        return -1;
    }

    if (lssdp->debug) {
        char str[INET6_ADDRSTRLEN];
        if (inet_ntop(AF_INET6, &address.sin6_addr, str, INET6_ADDRSTRLEN) == NULL) {
            lssdp_error("inet_ntop failed, errno = %s (%d)\n", strerror(errno), errno);
        } else {
            lssdp_info("RECV packet from %s : %s\n", str, buffer);
        }
    }

    // ignore the SSDP packet received from self
    size_t i;
    for (i = 0; i < lssdp->interface_num; i++) {
    //    if (lssdp->interface[i].addr == address.sin6_addr.s6_addr) {
    //        goto end;
    //    }
    }

    // parse SSDP packet to struct
    lssdp_packet packet = {};
    if (lssdp_packet_parser(buffer, recv_len, &packet) != 0) {
        goto end;
    }

    // check search target
    if (strcmp(packet.st, lssdp->header.search_target) != 0) {
        // search target is not match
        if (lssdp->debug) {
            lssdp_info("RECV <- %-8s   not match with %-14s %s\n", packet.method, lssdp->header.search_target, packet.location);
        }
        goto end;
    }

    // M-SEARCH: send RESPONSE back
    if (strcmp(packet.method, Global.MSEARCH) == 0) {
        lssdp_send_response(lssdp, address);
        goto end;
    }

    if (lssdp->debug) {
        lssdp_info("RECV <- %-8s   %-28s  %s\n", packet.method, packet.location, packet.sm_id);
    }

end:
    // invoke packet received callback
    if (lssdp->packet_received_callback != NULL) {
        lssdp->packet_received_callback(lssdp, buffer, recv_len);
    }

    return 0;
}

// 05. lssdp_send_msearch
int lssdp_send_msearch(lssdp_ctx * lssdp) {
    if (lssdp == NULL) {
        lssdp_error("lssdp should not be NULL\n");
        return -1;
    }

    if (lssdp->port == 0) {
        lssdp_error("SSDP port (%d) has not been setup.\n", lssdp->port);
        return -1;
    }

    // check network inerface number
    if (lssdp->interface_num == 0) {
        lssdp_warn("Network Interface is empty, no destination to send %s\n", Global.MSEARCH);
        return -1;
    }

    // 1. set M-SEARCH packet
    char msearch[LSSDP_BUFFER_LEN] = {};
    snprintf(msearch, sizeof(msearch),
        "%s"
        "HOST:%s:%d\r\n"
        "MAN:\"ssdp:discover\"\r\n"
        "MX:1\r\n"
        "ST:%s\r\n"
        "USER-AGENT:OS/version product/version\r\n"
        "\r\n",
        Global.HEADER_MSEARCH,              // HEADER
        Global.ADDR_MULTICAST, lssdp->port, // HOST
        lssdp->header.search_target         // ST (Search Target)
    );

    // 2. send M-SEARCH to each interface
    size_t i;
    for (i = 0; i < lssdp->interface_num; i++) {
        struct lssdp_interface * interface = &lssdp->interface[i];

        // avoid sending multicast to localhost
        __uint128_t a;
        if (inet_pton(AF_INET6, Global.ADDR_LOCALHOST, &a) == 0) {
            lssdp_error("inet_pton failed, errno = %s (%d)\n", strerror(errno), errno);
            continue;
        }

        if (interface->addr == a) {
            continue;
        }

        // send M-SEARCH
        int ret = send_multicast_data(msearch, *interface, lssdp->port);
        if (ret == 0 && lssdp->debug) {
            lssdp_info("SEND => %-8s   %s => MULTICAST\n", Global.MSEARCH, interface->ip);
        }
    }

    return 0;
}

// 06. lssdp_send_notify
int lssdp_send_notify(lssdp_ctx * lssdp) {
    if (lssdp == NULL) {
        lssdp_error("lssdp should not be NULL\n");
        return -1;
    }

    if (lssdp->port == 0) {
        lssdp_error("SSDP port (%d) has not been setup.\n", lssdp->port);
        return -1;
    }

    // check network inerface number
    if (lssdp->interface_num == 0) {
        lssdp_warn("Network Interface is empty, no destination to send %s\n", Global.NOTIFY);
        return -1;
    }

    size_t i;
    for (i = 0; i < lssdp->interface_num; i++) {
        struct lssdp_interface * interface = &lssdp->interface[i];

        // avoid sending multicast to localhost
        __uint128_t a;
        if (inet_pton(AF_INET6, Global.ADDR_LOCALHOST, &a) == 0) {
            lssdp_error("inet_pton failed, errno = %s (%d)\n", strerror(errno), errno);
            continue;
        }

        if (interface->addr == a) {
            continue;
        }

        // set notify packet
        char notify[LSSDP_BUFFER_LEN] = {};
        char * domain = lssdp->header.location.domain;
        snprintf(notify, sizeof(notify),
            "%s"
            "HOST:%s:%d\r\n"
            "CACHE-CONTROL:max-age=120\r\n"
            "LOCATION:%s%s%s\r\n"
            "SERVER:%s\r\n"
            "NT:%s\r\n"
            "NTS:ssdp:alive\r\n"
            "USN:%s\r\n"
            "SM_ID:%s\r\n"
            "DEV_TYPE:%s\r\n"
            "\r\n",
            Global.HEADER_NOTIFY,                       // HEADER
            Global.ADDR_MULTICAST, lssdp->port,         // HOST
            lssdp->header.location.prefix,              // LOCATION
            strlen(domain) > 0 ? domain : interface->ip,
            lssdp->header.location.suffix,
            lssdp->header.server,                       // SERVER
            lssdp->header.search_target,                // NT (Notify Type)
            lssdp->header.unique_service_name,          // USN
            lssdp->header.sm_id,                        // SM_ID    (addtional field)
            lssdp->header.device_type                   // DEV_TYPE (addtional field)
        );

        // send NOTIFY
        int ret = send_multicast_data(notify, *interface, lssdp->port);
        if (ret == 0 && lssdp->debug) {
            lssdp_info("SEND => %-8s   %s => MULTICAST\n", Global.NOTIFY, interface->ip);
        }
    }

    // network inerface is empty
    if (i == 0) lssdp_warn("Network Interface is empty, no destination to send %s\n", Global.NOTIFY);

    return 0;
}

// 08. lssdp_set_log_callback
void lssdp_set_log_callback(void (* callback)(const char * file, const char * tag, int level, int line, const char * func, const char * message)) {
    Global.log_callback = callback;
}


/** Internal Function **/

static int send_multicast_data(const char * data, const struct lssdp_interface interface, unsigned short ssdp_port) {
    if (data == NULL) {
        lssdp_error("data should not be NULL\n");
        return -1;
    }

    size_t data_len = strlen(data);
    if (data_len == 0) {
        lssdp_error("data length should not be empty\n");
        return -1;
    }

    if (strlen(interface.name) == 0) {
        lssdp_error("interface.name should not be empty\n");
        return -1;
    }

    int result = -1;
    sockaddr_in6 dest_addr{};
    sockaddr_in6 addr{};
    int opt;

    // 1. create UDP socket
    int fd = socket(PF_INET6, SOCK_DGRAM, 0);
    if (fd < 0) {
        lssdp_error("create socket failed, errno = %s (%d)\n", strerror(errno), errno);
        goto end;
    }

    // 2. bind socket
    addr.sin6_family      = AF_INET6;
    addr.sin6_scope_id    = if_nametoindex(interface.name);
    memcpy(addr.sin6_addr.s6_addr, &interface.addr, sizeof(addr.sin6_addr.s6_addr));
    if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        lssdp_error("bind failed, errno = %s (%d)\n", strerror(errno), errno);
        goto end;
    }

    // 3. disable IP_MULTICAST_LOOP
    opt = 0;
    if (setsockopt(fd, IPPROTO_IPV6, IPV6_MULTICAST_LOOP, &opt, sizeof(opt)) < 0) {
        lssdp_error("setsockopt IPV6_MULTICAST_LOOP failed, errno = %s (%d)\n", strerror(errno), errno);
        goto end;
    }

    // 4. set destination address
    dest_addr.sin6_family = AF_INET6;
    dest_addr.sin6_port = htons(ssdp_port);
    if (inet_pton(AF_INET6, Global.ADDR_MULTICAST, &dest_addr.sin6_addr) == 0) {
        lssdp_error("inet_aton failed, errno = %s (%d)\n", strerror(errno), errno);
        goto end;
    }

    // 5. send data
    if (sendto(fd, data, data_len, 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr)) == -1) {
        lssdp_error("sendto %s (%s) failed, errno = %s (%d)\n", interface.name, interface.ip, strerror(errno), errno);
        goto end;
    }

    result = 0;
end:
    if (fd >= 0 && close(fd) != 0) {
        lssdp_error("close fd %d failed, errno = %s (%d)\n", strerror(errno), errno);
    }
    return result;
}

static int lssdp_send_response(lssdp_ctx * lssdp, struct sockaddr_in6 address) {
    // get M-SEARCH IP
    char msearch_ip[LSSDP_IP_LEN] = {};
    if (inet_ntop(AF_INET6, &address.sin6_addr, msearch_ip, sizeof(msearch_ip)) == NULL) {
        lssdp_error("inet_ntop failed, errno = %s (%d)\n", strerror(errno), errno);
        return -1;
    }

    // 1. find the interface which is in LAN
    struct lssdp_interface * interface = find_interface_in_LAN(lssdp, address);
    if (interface == NULL) {
        if (lssdp->debug) {
            lssdp_info("RECV <- %-8s   Interface is not found        %s\n", Global.MSEARCH, msearch_ip);
        }

        if (lssdp->interface_num == 0) {
            lssdp_warn("Network Interface is empty, no destination to send %s\n", Global.RESPONSE);
        }
        return -1;
    }

    // 2. set response packet
    char response[LSSDP_BUFFER_LEN] = {};
    char * domain = lssdp->header.location.domain;
    int response_len = snprintf(response, sizeof(response),
        "%s"
        "CACHE-CONTROL:max-age=120\r\n"
        "DATE:\r\n"
        "EXT:\r\n"
        "LOCATION:%s%s%s\r\n"
        "SERVER:%s\r\n"
        "ST:%s\r\n"
        "USN:%s\r\n"
        "SM_ID:%s\r\n"
        "DEV_TYPE:%s\r\n"
        "\r\n",
        Global.HEADER_RESPONSE,                     // HEADER
        lssdp->header.location.prefix,              // LOCATION
        strlen(domain) > 0 ? domain : interface->ip,
        lssdp->header.location.suffix,
        lssdp->header.server,                       // SERVER
        lssdp->header.search_target,                // ST (Search Target)
        lssdp->header.unique_service_name,          // USN
        lssdp->header.sm_id,                        // SM_ID    (addtional field)
        lssdp->header.device_type                   // DEV_TYPE (addtional field)
    );

    // 3. set port to address
    address.sin6_port = htons(lssdp->port);

    if (lssdp->debug) {
        lssdp_info("RECV <- %-8s   %s <- %s\n", Global.MSEARCH, interface->ip, msearch_ip);
    }

    // 4. send data
    if (sendto(lssdp->sock, response, response_len, 0, (struct sockaddr *)&address, sizeof(struct sockaddr_in6)) == -1) {
        lssdp_error("send RESPONSE to %s failed, errno = %s (%d)\n", msearch_ip, strerror(errno), errno);
        return -1;
    }

    if (lssdp->debug) {
        lssdp_info("SEND => %-8s   %s => %s\n", Global.RESPONSE, interface->ip, msearch_ip);
    }

    return 0;
}

static int lssdp_packet_parser(const char * data, size_t data_len, lssdp_packet * packet) {
    if (data == NULL) {
        lssdp_error("data should not be NULL\n");
        return -1;
    }

    if (data_len != strlen(data)) {
        lssdp_error("data_len (%zu) is not match to the data length (%zu)\n", data_len, strlen(data));
        return -1;
    }

    if (packet == NULL) {
        lssdp_error("packet should not be NULL\n");
        return -1;
    }

    // 1. compare SSDP Method Header: M-SEARCH, NOTIFY, RESPONSE
    size_t i;
    if ((i = strlen(Global.HEADER_MSEARCH)) < data_len && memcmp(data, Global.HEADER_MSEARCH, i) == 0) {
        strcpy(packet->method, Global.MSEARCH);
    } else if ((i = strlen(Global.HEADER_NOTIFY)) < data_len && memcmp(data, Global.HEADER_NOTIFY, i) == 0) {
        strcpy(packet->method, Global.NOTIFY);
    } else if ((i = strlen(Global.HEADER_RESPONSE)) < data_len && memcmp(data, Global.HEADER_RESPONSE, i) == 0) {
        strcpy(packet->method, Global.RESPONSE);
    } else {
        lssdp_warn("received unknown SSDP packet\n");
        lssdp_debug("%s\n", data);
        return -1;
    }

    // 2. parse each field line
    size_t start = i;
    for (i = start; i < data_len; i++) {
        if (data[i] == '\n' && i - 1 > start && data[i - 1] == '\r') {
            parse_field_line(data, start, i - 2, packet);
            start = i + 1;
        }
    }

    // 3. set update_time
    long long current_time = get_current_time_();
    if (current_time < 0) {
        lssdp_error("got invalid timestamp %lld\n", current_time);
        return -1;
    }
    packet->update_time = current_time;
    return 0;
}

static int parse_field_line(const char * data, size_t start, size_t end, lssdp_packet * packet) {
    // 1. find the colon
    if (data[start] == ':') {
        lssdp_warn("the first character of line should not be colon\n");
        lssdp_debug("%s\n", data);
        return -1;
    }

    int colon = get_colon_index(data, start + 1, end);
    if (colon == -1) {
        lssdp_warn("there is no colon in line\n");
        lssdp_debug("%s\n", data);
        return -1;
    }

    if (colon == end) {
        // value is empty
        return -1;
    }


    // 2. get field, field_len
    size_t i = start;
    size_t j = colon - 1;
    if (trim_spaces(data, &i, &j) == -1) {
        return -1;
    }
    const char * field = &data[i];
    size_t field_len = j - i + 1;


    // 3. get value, value_len
    i = colon + 1;
    j = end;
    if (trim_spaces(data, &i, &j) == -1) {
        return -1;
    };
    const char * value = &data[i];
    size_t value_len = j - i + 1;


    // 4. set each field's value to packet
    if (field_len == strlen("st") && strncasecmp(field, "st", field_len) == 0) {
        memcpy(packet->st, value, value_len < LSSDP_FIELD_LEN ? value_len : LSSDP_FIELD_LEN - 1);
        return 0;
    }

    if (field_len == strlen("nt") && strncasecmp(field, "nt", field_len) == 0) {
        memcpy(packet->st, value, value_len < LSSDP_FIELD_LEN ? value_len : LSSDP_FIELD_LEN - 1);
        return 0;
    }

    if (field_len == strlen("usn") && strncasecmp(field, "usn", field_len) == 0) {
        memcpy(packet->usn, value, value_len < LSSDP_FIELD_LEN ? value_len : LSSDP_FIELD_LEN - 1);
        return 0;
    }

    if (field_len == strlen("location") && strncasecmp(field, "location", field_len) == 0) {
        memcpy(packet->location, value, value_len < LSSDP_LOCATION_LEN ? value_len : LSSDP_LOCATION_LEN - 1);
        return 0;
    }

    if (field_len == strlen("sm_id") && strncasecmp(field, "sm_id", field_len) == 0) {
        memcpy(packet->sm_id, value, value_len < LSSDP_FIELD_LEN ? value_len : LSSDP_FIELD_LEN - 1);
        return 0;
    }

    if (field_len == strlen("dev_type") && strncasecmp(field, "dev_type", field_len) == 0) {
        memcpy(packet->device_type, value, value_len < LSSDP_FIELD_LEN ? value_len : LSSDP_FIELD_LEN - 1);
        return 0;
    }

    if (field_len == strlen("server") && strncasecmp(field, "server", field_len) == 0) {
        memcpy(packet->server, value, value_len < LSSDP_FIELD_LEN ? value_len : LSSDP_FIELD_LEN - 1);
        return 0;
    }

    // the field is not in the struct packet
    return 0;
}

static int get_colon_index(const char * string, size_t start, size_t end) {
    size_t i;
    for (i = start; i <= end; i++) {
        if (string[i] == ':') {
            return i;
        }
    }
    return -1;
}

static int trim_spaces(const char * string, size_t * start, size_t * end) {
    int i = *start;
    int j = *end;

    while (i <= *end   && (!isprint(string[i]) || isspace(string[i]))) i++;
    while (j >= *start && (!isprint(string[j]) || isspace(string[j]))) j--;

    if (i > j) {
        return -1;
    }

    *start = i;
    *end   = j;
    return 0;
}

static long long get_current_time_() {
    struct timeval time = {};
    if (gettimeofday(&time, NULL) == -1) {
        lssdp_error("gettimeofday failed, errno = %s (%d)\n", strerror(errno), errno);
        return -1;
    }
    return (long long) time.tv_sec * 1000 + (long long) time.tv_usec / 1000;
}

static int lssdp_log(int level, int line, const char * func, const char * format, ...) {
    if (Global.log_callback == NULL) {
        return -1;
    }

    char message[LSSDP_BUFFER_LEN] = {};

    // create message by va_list
    va_list args;
    va_start(args, format);
    vsnprintf(message, LSSDP_BUFFER_LEN, format, args);
    va_end(args);

    // invoke log callback function
    Global.log_callback(__FILE__, "SSDP", level, line, func, message);
    return 0;
}

static struct lssdp_interface * find_interface_in_LAN(lssdp_ctx * lssdp, sockaddr_in6 address) {
    __uint128_t address_int;
    memcpy(&address_int, address.sin6_addr.s6_addr, 16);

    struct lssdp_interface * ifc;
    size_t i;
    for (i = 2; i < lssdp->interface_num; i++) {
        ifc = &lssdp->interface[i];

        // mask address to check whether the interface is under the same Local Network Area or not
        if ((ifc->addr & ifc->netmask) == (address_int & ifc->netmask)) {
            return ifc;
        }
    }
    return NULL;
}
