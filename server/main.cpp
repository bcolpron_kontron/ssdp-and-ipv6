#include "httpserver/httpserver.h"
#include <nlohmann/json.hpp>
#include "common/ssdp.h"

using json = nlohmann::json;

void log_callback(const char * file, const char * tag, int level, int line, const char * func, const char * message) {
    const char * level_name = "DEBUG";
    if (level == LSSDP_LOG_INFO)   level_name = "INFO";
    if (level == LSSDP_LOG_WARN)   level_name = "WARN";
    if (level == LSSDP_LOG_ERROR)  level_name = "ERROR";

    printf("[%-5s][%s] %s", level_name, tag, message);
}

int show_interface_list_and_rebind_socket(lssdp_ctx * lssdp) {
    // 1. show interface list
    printf("\nNetwork Interface List (%zu):\n", lssdp->interface_num);
    size_t i;
    for (i = 0; i < lssdp->interface_num; i++) {
        printf("%zu. %-6s: %s\n",
            i + 1,
            lssdp->interface[i].name,
            lssdp->interface[i].ip
        );
    }
    printf("%s\n", i == 0 ? "Empty" : "");

    // 2. re-bind SSDP socket
    if (lssdp_socket_create(lssdp) != 0) {
        puts("SSDP create socket failed");
        return -1;
    }

    return 0;
}

std::function<void(const lssdp_packet&)> callback = [](const auto&) {};

int show_ssdp_packet(struct lssdp_ctx * lssdp, const char * packet, size_t packet_len) {

    lssdp_packet msg = {};
    if (lssdp_packet_parser(packet, packet_len, &msg) != 0) {
        return 1;
    }
    printf("%s %s %s\n", msg.st, msg.usn, msg.location);
    callback(msg);
    return 0;
}

long long get_current_time() {
    struct timeval time = {};
    if (gettimeofday(&time, NULL) == -1) {
        printf("gettimeofday failed, errno = %s (%d)\n", strerror(errno), errno);
        return -1;
    }
    return (long long) time.tv_sec * 1000 + (long long) time.tv_usec / 1000;
}



int main(int, const char**)
{
    HttpServer server(8315);
    server.add_http_handler(http::verb::get, "/test/?", [](auto&& req)
    {
        return make_response(req, "Hello\n");
    });
    server.add_ws_handler("/ws(/.*)?", [&](auto msg, auto& session) {
        std::cout << msg << std::endl;
        // echo the message to all clients
        for(auto& session: server.get_ws_sessions()) session->send(msg);
    });
    server.serve_files("/", "../www/");

    std::cout << "Server started" << std::endl; 
    server.start();


    callback = [&] (const auto& msg) {

        json json;
        json["location"] = msg.location;
        json["service_type"] = msg.st;
        json["unique_service_name"] = msg.usn;
        json["server"] = msg.server;

        for(auto& session: server.get_ws_sessions()) session->send(json.dump());
    };





    lssdp_ctx lssdp;
    lssdp.port = 1900;
    lssdp.debug = true;
    lssdp.network_interface_changed_callback = show_interface_list_and_rebind_socket;
    lssdp.packet_received_callback           = show_ssdp_packet;
    strcpy(lssdp.header.search_target, "generic:server");

    lssdp_set_log_callback(log_callback);

    /* get network interface at first time, network_interface_changed_callback will be invoke
     * SSDP socket will be created in callback function
     */
    lssdp_network_interface_update(&lssdp);

    long long last_time = get_current_time();
    if (last_time < 0) {
        printf("got invalid timestamp %lld\n", last_time);
        return EXIT_SUCCESS;
    }

    // Main Loop
    for (;;) {
        fd_set fs;
        FD_ZERO(&fs);
        FD_SET(lssdp.sock, &fs);
        timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = 500 * 1000;   // 500 ms

        int ret = select(lssdp.sock + 1, &fs, NULL, NULL, &tv);
        if (ret < 0) {
            printf("select error, ret = %d\n", ret);
            break;
        }

        if (ret > 0) {
            lssdp_socket_read(&lssdp);
        }

                // get current time
        long long current_time = get_current_time();
        if (current_time < 0) {
            printf("got invalid timestamp %lld\n", current_time);
            break;
        }

        // doing task per 5 seconds
        if (current_time - last_time >= 5000) {
            lssdp_send_msearch(&lssdp);             // 2. send M-SEARCH
            last_time = current_time;               // update last_time
        }
    }


    return 0;
}
